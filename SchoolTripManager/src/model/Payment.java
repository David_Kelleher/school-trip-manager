package model;

import java.sql.*;

public class Payment {
	
	private int paymentId;
	private java.sql.Timestamp paymentDateTime;
	private double amount;
	private int studentId;
	private String studentName;
	private int tripId;
	private String tripName;
	
	//Create constructors
	public Payment() {
		
	}
	
	public Payment(int paymentId, java.sql.Timestamp paymentDateTime, double amount, int studentId, String studentName,
			int tripId, String tripName) {
		super();
		this.paymentId = paymentId;
		this.paymentDateTime = paymentDateTime;
		this.amount = amount;
		this.studentId = studentId;
		this.studentName = studentName;
		this.tripId = tripId;
		this.tripName = tripName;
	}
	
	
	//Create getters and setters
	public int getPaymentId() {
		return paymentId;
	}
	public void setPaymentId(int paymentId) {
		this.paymentId = paymentId;
	}
	public java.sql.Timestamp getPaymentDateTime() {
		return paymentDateTime;
	}
	public void setPaymentDateTime(java.sql.Timestamp paymentDateTime) {
		this.paymentDateTime = paymentDateTime;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public int getTripId() {
		return tripId;
	}
	public void setTripId(int tripId) {
		this.tripId = tripId;
	}
	public String getTripName() {
		return tripName;
	}
	public void setTripName(String tripName) {
		this.tripName = tripName;
	}
	
	
	
}
