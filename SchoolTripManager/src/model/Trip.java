package model;

import java.util.Date;
import java.sql.*;

public class Trip {

	private int tripId;
	private String tripName;
	private java.sql.Date startDate;
	private java.sql.Date endDate;
	private String keyTeacher;
	private double totalCost;
	private java.sql.Date deadline;
	
	
	// Create constructors
	public Trip() {
		
	}
	
	public Trip(String tripName, java.sql.Date startDate, java.sql.Date endDate, String keyTeacher, double totalCost,
			java.sql.Date deadline) {
		super();
		this.tripName = tripName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.keyTeacher = keyTeacher;
		this.totalCost = totalCost;
		this.deadline = deadline;
	}

	//Create getters and setters
	public int getTripId() {
		return tripId;
	}


	public void setTripId(int tripId) {
		this.tripId = tripId;
	}


	public String getTripName() {
		return tripName;
	}


	public void setTripName(String tripName) {
		this.tripName = tripName;
	}


	public java.sql.Date getStartDate() {
		return startDate;
	}


	public void setStartDate(java.sql.Date startDate) {
		this.startDate = startDate;
	}


	public java.sql.Date getEndDate() {
		return endDate;
	}


	public void setEndDate(java.sql.Date endDate) {
		this.endDate = endDate;
	}


	public String getKeyTeacher() {
		return keyTeacher;
	}


	public void setKeyTeacher(String keyTeacher) {
		this.keyTeacher = keyTeacher;
	}


	public double getTotalCost() {
		return totalCost;
	}


	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}


	public java.sql.Date getDeadline() {
		return deadline;
	}


	public void setDeadline(java.sql.Date deadline) {
		this.deadline = deadline;
	}
	
	
}
