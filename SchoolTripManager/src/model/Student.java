package model;

public class Student {
	
	private int studentId;
	private int studentName;
	
	//Constructors
	public Student() {
		
	}
	
	public Student(int studentId, int studentName) {
		super();
		this.studentId = studentId;
		this.studentName = studentName;
	}
	//Getters, setters
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	public int getStudentName() {
		return studentName;
	}
	public void setStudentName(int studentName) {
		this.studentName = studentName;
	}
	
	

}
