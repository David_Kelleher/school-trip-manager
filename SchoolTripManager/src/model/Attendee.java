package model;

public class Attendee {
	
	private int tripId;
	private int studentId;
	private String tripName;
	private String studentName;
	private double outStandingFees;
	
	//Create constructors
	public Attendee() {
		
	}
	
	public Attendee(int tripId, int studentId, String tripName, String studentName, double outStandingFees) {
		super();
		this.tripId = tripId;
		this.studentId = studentId;
		this.tripName = tripName;
		this.studentName = studentName;
		this.outStandingFees = outStandingFees;
	}

	//Create getters and setters
	public int getTripId() {
		return tripId;
	}

	public void setTripId(int tripId) {
		this.tripId = tripId;
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getTripName() {
		return tripName;
	}

	public void setTripName(String tripName) {
		this.tripName = tripName;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public double getOutStandingFees() {
		return outStandingFees;
	}

	public void setOutStandingFees(double outStandingFees) {
		this.outStandingFees = outStandingFees;
	}

}
