package main;
import java.util.Scanner;
import java.text.DateFormat;
import java.text.ParseException; 
import java.sql.*;
import java.time.LocalDate;
import java.text.SimpleDateFormat;

import dao.*;
import model.Attendee;
import model.Payment;
import model.Trip;


public class Main {

	public static void main(String[] args) {
		
		//Create required objects
		Scanner sc = new Scanner(System.in);
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
		
		System.out.println("Hi, Welcome to the School Trip Manager.");
		System.out.println();
		System.out.println("Please select an option below.");
		System.out.println();
		System.out.println("1. MAKE A PAYMENT");
		System.out.println("2. VIEW PREVIOUS PAYMENTS AND OUTSTANDING BALANCE");
		System.out.println("3. VIEW ALL UPCOMING TRIPS");
		System.out.println("--------------------------");
		System.out.println("4. CREATE NEW TRIP (STAFF)");
		System.out.println("5. ADD/REMOVE STUDENT TO/FROM TRIPS (STAFF)");
		System.out.println("6. VIEW TRIP SUMMARY (STAFF)");
		System.out.println("7. CHANGE PASSWORD (STAFF)");
		
		int userSelect = sc.nextInt();
		
		switch (userSelect) {
		case 1:
			//Create new payment model object
			Payment payment = new Payment();
			//create timestamp to pass to db
			Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			//Set time of payment
			payment.setPaymentDateTime(timestamp);
			//Prompt user for student id
			System.out.println("Please enter your student id:");
			payment.setStudentId(sc.nextInt());
			//Prompt user for tripID
			System.out.println("Please enter the ID of the trip you're paying for:");
			payment.setTripId(sc.nextInt());
			//Prompt user for payment amount
			System.out.println("Please enter the amount you're paying today:");
			payment.setAmount(sc.nextDouble());
			
			//Update datebase
			DBUpdatePayment dBUpdatePayment = new DBUpdatePayment();
			dBUpdatePayment.insertPayment(payment);
			
			
			break;
			
		case 2:
			
			break;
			
		case 3:
			DBShowAllTrips dBshowAllTrips = new DBShowAllTrips(); 
			System.out.println(); //Print a blank line
			System.out.println(dBshowAllTrips.getData());
			break;
			
		case 4:
			Trip trip = new Trip();
			//Prompt user for trip details and store in variables
			System.out.println("Enter Trip Name:");
			sc.nextLine();
			String tripName = sc.nextLine();
			trip.setTripName(tripName);
			System.out.println("You entered: " + tripName);
			
			System.out.println("Enter Start date...");
			System.out.println("Year (YYYY):");
			int startYear = sc.nextInt();
			System.out.println("Month (MM):");
			int startMonth = sc.nextInt();
			System.out.println("Day (DD):");
			int startDay = sc.nextInt();
			java.sql.Date Startdate = new Date(startYear, startMonth, startDay);
			trip.setStartDate(Startdate);

			
			System.out.println("Enter End date...");
			System.out.println("Year (YYYY):");
			int endYear = sc.nextInt();
			System.out.println("Month (MM):");
			int endMonth = sc.nextInt();
			System.out.println("Day (DD):");
			int endDay = sc.nextInt();
			java.sql.Date endDate = new Date(endYear, endMonth, endDay);
			trip.setEndDate(endDate);
						
			System.out.println("Enter key teacher: ");
			sc.nextLine();
			String keyTeacher = sc.nextLine();
			trip.setKeyTeacher(keyTeacher);
			
			System.out.println("Enter total cost: ");
			double totalCost = sc.nextDouble();
			trip.setTotalCost(totalCost);
			
			
			System.out.println("Enter payment deadline...");
			sc.nextLine();
			System.out.println("Year (YYYY):");
			int deadlineYear = sc.nextInt();
			System.out.println("Month (MM):");
			int deadlineMonth = sc.nextInt();
			System.out.println("Day (DD):");
			int deadlineDay = sc.nextInt();
			java.sql.Date deadlineDate = new Date(deadlineYear, deadlineMonth, deadlineDay);
			trip.setDeadline(deadlineDate);
			
			//Create DAO instance and use method to update database
			DBCreateNewTrip dBCreateNewTrip = new DBCreateNewTrip(); 
		
			
			dBCreateNewTrip.insertData(trip.getTripName(), trip.getStartDate(), 
					trip.getEndDate(), trip.getKeyTeacher(), trip.getTotalCost(), trip.getDeadline());
			break;
			
		case 5:
			Attendee attendee = new Attendee();
			System.out.println("Please enter trip ID:");
			attendee.setTripId(sc.nextInt());
			System.out.println("Please enter Student ID:");
			attendee.setStudentId(sc.nextInt());
			
			//Create DAO and use methods to set relevant values retrieved from trips table 
			DBAddAttendee dBAddAttendee = new DBAddAttendee();
			dBAddAttendee.updateStudentName(attendee);
			dBAddAttendee.updateTripName(attendee);
			attendee.setOutStandingFees(dBAddAttendee.setTripCost(attendee.getTripId()));
			dBAddAttendee.addAttendee(attendee);
			System.out.println("Success!");
			break;
			
		case 6:
			DBViewAllAttendees dBViewAllAttendees = new DBViewAllAttendees();
			System.out.println("Enter trip ID to show attendee list:");
			int tripID = sc.nextInt();
			System.out.println(dBViewAllAttendees.getData(tripID));
			break;
			
		case 7:
			
			break;
		}
	}

}
