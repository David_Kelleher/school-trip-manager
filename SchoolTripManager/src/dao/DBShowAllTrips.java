package dao;

import java.sql.*;

public class DBShowAllTrips {
	
	//Returns a customized string detailing all data from Trips table
	public String getData() {
		
	String result = "";
	
	//Save location and credentials of database
	String url = "jdbc:mysql://localhost:3306/";
	String dbname = "School_Trip_Manager";
	String username = "root";
	String password = "admin";
	
	Connection con = null;
	Statement stmt = null;
	ResultSet rs = null;
	
	// Establish a connection to db, query db and save results in result set
	try {
		con = DriverManager.getConnection(url+dbname, username, password);
		
		String query = "SELECT * FROM TRIPS";
		stmt = con.createStatement();
		rs = stmt.executeQuery(query);
		
		//Store result set in variable to be returned by method
		while (rs.next()) {
			result += "TRIP ID: " + rs.getString("Trip_ID") + "\n" + "TRIP NAME: " + rs.getString("Trip_Name") + "\n"
					+ "START DATE: " + rs.getString("Start_Date") + "\n" + "END DATE: " + rs.getString("End_Date") + "\n"
					+ "KEY TEACHER: " + rs.getString("Key_Teacher") + "\n" + "TOTAL COST: " + rs.getString("Total_Cost") + "\n"
					+ "PAYMENT DEADLINE: " + rs.getString("Payment_Deadline") + "\n" + "*****************************" + "\n";
		}
		
	}
	catch (SQLException e) {e.printStackTrace();}
	catch (Exception e) {e.printStackTrace();}
	
	finally {
		if (rs != null) {
			try {rs.close();} catch (Exception e) {e.printStackTrace();}
		}
		if (stmt != null) {
			try {stmt.close();} catch (Exception e) {e.printStackTrace();}
		}
		if (con != null) {
			try {con.close();} catch (Exception e) {e.printStackTrace();}
		}
	}
	
	return result;
	
	

}
}



	
