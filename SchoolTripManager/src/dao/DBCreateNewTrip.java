package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Date;
import java.text.SimpleDateFormat; 

public class DBCreateNewTrip {
	
	public void insertData(String Trip_Name, java.sql.Date Start_Date, java.sql.Date End_Date, String Key_Teacher,
			double Total_Cost, java.sql.Date Deadline_Date) {
		


		//Save location and credentials of database
		String url = "jdbc:mysql://localhost:3306/";
		String dbname = "School_Trip_Manager";
		String username = "root";
		String password = "admin";
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			con = DriverManager.getConnection(url+dbname, username, password);
			
			String query = "INSERT INTO TRIPS (Trip_ID, Trip_Name, Start_Date, End_Date, Key_Teacher, " +
			"Total_Cost, Payment_Deadline) VALUES (NULL, ? , ?, ?, ?, ?, ?);";
			
			pstmt = con.prepareStatement(query);
			
			pstmt.setString(1, Trip_Name);
			pstmt.setDate(2, Start_Date);
			pstmt.setDate(3, End_Date);
			pstmt.setString(4, Key_Teacher);
			pstmt.setDouble(5, Total_Cost);
			pstmt.setDate(6, Deadline_Date);
			
			pstmt.executeUpdate();
			int rowsAffected = pstmt.getUpdateCount();
			

	}
		catch (SQLException e) {e.printStackTrace();}
		catch (Exception e) {e.printStackTrace();}
		
		finally {
			if (rs != null) {
				try {rs.close();} catch (Exception e) {e.printStackTrace();}
			}
			if (pstmt != null) {
				try {pstmt.close();} catch (Exception e) {e.printStackTrace();}
			}
			if (con != null) {
				try {con.close();} catch (Exception e) {e.printStackTrace();}
			}
		}
		
		
}

}

