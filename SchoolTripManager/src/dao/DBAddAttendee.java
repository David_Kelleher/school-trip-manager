package dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.Attendee;
import model.Payment;

public class DBAddAttendee {
	
	
	//this method queries database for name relating to student number entered
	//updates the attendee object to include valid student name
	//so that student name is included in attendee details passed to db
	public void updateStudentName(Attendee attendee) {
		String url = "jdbc:mysql://localhost:3306/";
		String dbname = "School_Trip_Manager";
		String username = "root";
		String password = "admin";
		
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		try {
			con = DriverManager.getConnection(url+dbname, username, password);
			
			String queryStudentName = "Select Student_Name FROM Students WHERE Student_ID = " + attendee.getStudentId();
			stmt = con.createStatement();
			rs = stmt.executeQuery(queryStudentName);
			
			while (rs.next()) {
				attendee.setStudentName(rs.getString("Student_Name"));
			}
			
	}
		catch (SQLException e) {e.printStackTrace();}
		catch (Exception e) {e.printStackTrace();}
		
		finally {
			if (rs != null) {
				try {rs.close();} catch (Exception e) {e.printStackTrace();}
			}
			if (stmt != null) {
				try {stmt.close();} catch (Exception e) {e.printStackTrace();}
			}
			if (con != null) {
				try {con.close();} catch (Exception e) {e.printStackTrace();}
			}

	}
	}
	
	//this method queries database for name relating to trip number entered
	//updates the attendee object to include valid trip name
	//so that trip name is included in attendee details passed to db
	public void updateTripName(Attendee attendee) {
		String url = "jdbc:mysql://localhost:3306/";
		String dbname = "School_Trip_Manager";
		String username = "root";
		String password = "admin";
		
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		try {
			con = DriverManager.getConnection(url+dbname, username, password);
			
			String queryTripName = "Select Trip_Name FROM Trips WHERE Trip_ID = " + attendee.getTripId();
			stmt = con.createStatement();
			rs = stmt.executeQuery(queryTripName);
			
			rs.next();
			String tripName = rs.getString("Trip_Name");
			attendee.setTripName(tripName);
			
			
	}
		catch (SQLException e) {e.printStackTrace();}
		catch (Exception e) {e.printStackTrace();}
		
		finally {
			if (rs != null) {
				try {rs.close();} catch (Exception e) {e.printStackTrace();}
			}
			if (stmt != null) {
				try {stmt.close();} catch (Exception e) {e.printStackTrace();}
			}
			if (con != null) {
				try {con.close();} catch (Exception e) {e.printStackTrace();}
			}

	}
	}
	
	//This method queries trips table to get total trip cost
	public double setTripCost(int TripId) {
		double tripCost = 0;
		String url = "jdbc:mysql://localhost:3306/";
		String dbname = "School_Trip_Manager";
		String username = "root";
		String password = "admin";
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			con = DriverManager.getConnection(url+dbname, username, password);
			
			String queryTripName = "Select Total_Cost FROM Trips WHERE Trip_ID = ?";
			pstmt = con.prepareStatement(queryTripName);
			pstmt .setInt(1, TripId);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				tripCost = rs.getDouble("Total_Cost");
			}
			
	}
		catch (SQLException e) {e.printStackTrace();}
		catch (Exception e) {e.printStackTrace();}
		
		finally {
			if (rs != null) {
				try {rs.close();} catch (Exception e) {e.printStackTrace();}
			}
			if (pstmt != null) {
				try {pstmt.close();} catch (Exception e) {e.printStackTrace();}
			}
			if (con != null) {
				try {con.close();} catch (Exception e) {e.printStackTrace();}
			}

	}
		return tripCost;
	}
	
	public void addAttendee(Attendee attendee) {
		String url = "jdbc:mysql://localhost:3306/";
		String dbname = "School_Trip_Manager";
		String username = "root";
		String password = "admin";
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			con = DriverManager.getConnection(url+dbname, username, password);
			
			String update = "INSERT INTO attendees (Trip_ID, Student_ID, Trip_Name, Student_Name,"
					+ "Outstanding_fees) VALUES (? , ?, ?, ?, ?);";
			pstmt = con.prepareStatement(update);
			
			pstmt.setInt(1, attendee.getTripId());
			pstmt.setInt(2, attendee.getStudentId());
			pstmt.setString(3, attendee.getTripName());
			pstmt.setString(4, attendee.getStudentName());
			pstmt.setDouble(5, attendee.getOutStandingFees());
			
			pstmt.executeUpdate();
			

	}
		catch (SQLException e) {e.printStackTrace();} 
		catch (Exception e) {e.printStackTrace();}
		
		finally {
			if (rs != null) {
				try {rs.close();} catch (Exception e) {e.printStackTrace();}
			}
			if (pstmt != null) {
				try {pstmt.close();} catch (Exception e) {e.printStackTrace();}
			}
			if (con != null) {
				try {con.close();} catch (Exception e) {e.printStackTrace();}
			}

	}
		
	}
}
