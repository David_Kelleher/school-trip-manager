package dao;

import java.sql.*;

import model.Payment;



public class DBUpdatePayment {
	
	//this method queries database for name relating to student number entered
	//updates the payment object to include valid name
	//so that name is included in payment details passed to db
	public void updateStudentName(Payment payment) {
		String url = "jdbc:mysql://localhost:3306/";
		String dbname = "School_Trip_Manager";
		String username = "root";
		String password = "admin";
		
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		try {
			con = DriverManager.getConnection(url+dbname, username, password);
			
			String queryStudentName = "Select Student_Name FROM Students WHERE Student_ID = " + payment.getStudentId();
			stmt = con.createStatement();
			rs = stmt.executeQuery(queryStudentName);
			
			while (rs.next()) {
				payment.setStudentName(rs.getString("Student_Name"));
			}
			
	}
		catch (SQLException e) {e.printStackTrace();}
		catch (Exception e) {e.printStackTrace();}
		
		finally {
			if (rs != null) {
				try {rs.close();} catch (Exception e) {e.printStackTrace();}
			}
			if (stmt != null) {
				try {stmt.close();} catch (Exception e) {e.printStackTrace();}
			}
			if (con != null) {
				try {con.close();} catch (Exception e) {e.printStackTrace();}
			}

	}
	}
	
	//this method queries database for name relating to trip number entered
	//updates the payment object to include valid trip name
	//so that trip name is included in payment details passed to db
	public void updateTripName(Payment payment) {
		String url = "jdbc:mysql://localhost:3306/";
		String dbname = "School_Trip_Manager";
		String username = "root";
		String password = "admin";
		
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		try {
			con = DriverManager.getConnection(url+dbname, username, password);
			
			String queryTripName = "Select Trip_Name FROM Trips WHERE Trip_ID = " + payment.getTripId();
			stmt = con.createStatement();
			rs = stmt.executeQuery(queryTripName);
			
			while (rs.next()) {
				payment.setTripName(rs.getString("Trip_Name"));
			}
			
	}
		catch (SQLException e) {e.printStackTrace();}
		catch (Exception e) {e.printStackTrace();}
		
		finally {
			if (rs != null) {
				try {rs.close();} catch (Exception e) {e.printStackTrace();}
			}
			if (stmt != null) {
				try {stmt.close();} catch (Exception e) {e.printStackTrace();}
			}
			if (con != null) {
				try {con.close();} catch (Exception e) {e.printStackTrace();}
			}

	}
	}
	
	//this method passes the payment object details to database payments table
	public void insertPayment(Payment payment) {
		
		this.updateStudentName(payment);
		this.updateTripName(payment);
		
		//Save location and credentials of database
				String url = "jdbc:mysql://localhost:3306/";
				String dbname = "School_Trip_Manager";
				String username = "root";
				String password = "admin";
				
				Connection con = null;
				Statement stmt = null;
				PreparedStatement pstmt = null;
				ResultSet rs = null;
				
				try {
					con = DriverManager.getConnection(url+dbname, username, password);
	
					String update = "INSERT INTO Payments (Payment_ID, Payment_Date_Time, Amount, Student_ID, " +
					" Student_Name, Trip_ID, Trip_Name) VALUES (NULL, ? , ?, ?, ?, ?, ?);";
					
					pstmt = con.prepareStatement(update);
					
					
					
					pstmt.setTimestamp(1, payment.getPaymentDateTime());
					pstmt.setDouble(2, payment.getAmount());
					pstmt.setInt(3, payment.getStudentId());
					pstmt.setString(4, payment.getStudentName());
					pstmt.setDouble(5, payment.getTripId());
					pstmt.setString(6, payment.getTripName());
					
					pstmt.executeUpdate();
					System.out.println("Success!");
					

			}
				catch (SQLException e) {e.printStackTrace();}
				catch (Exception e) {e.printStackTrace();}
				
				finally {
					if (rs != null) {
						try {rs.close();} catch (Exception e) {e.printStackTrace();}
					}
					if (stmt != null) {
						try {stmt.close();} catch (Exception e) {e.printStackTrace();}
					}
					if (con != null) {
						try {con.close();} catch (Exception e) {e.printStackTrace();}
					}
				}
}
	
	public void updateOutstandingBalance(int Student_ID, int Trip_ID, double amount) {
		String url = "jdbc:mysql://localhost:3306/";
		String dbname = "School_Trip_Manager";
		String username = "root";
		String password = "admin";
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			con = DriverManager.getConnection(url+dbname, username, password);
			
			String update = "UPDATE attendees SET Outstanding_fees=Outstanding_fees-? WHERE Student_ID = ? AND WHERE Trip_ID=?";
			
			pstmt = con.prepareStatement(update);
			
			pstmt.setDouble(1, amount);
			pstmt.setInt(2, Student_ID);
			pstmt.setInt(3, Trip_ID);
			
			pstmt.executeUpdate();
		}
		catch (SQLException e) {e.printStackTrace();} 
		catch (Exception e) {e.printStackTrace();}
		
		finally {
			if (rs != null) {
				try {rs.close();} catch (Exception e) {e.printStackTrace();}
			}
			if (pstmt != null) {
				try {pstmt.close();} catch (Exception e) {e.printStackTrace();}
			}
			if (con != null) {
				try {con.close();} catch (Exception e) {e.printStackTrace();}
			}
		}
	}
}


