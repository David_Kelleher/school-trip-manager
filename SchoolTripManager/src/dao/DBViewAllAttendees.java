package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBViewAllAttendees {
	
	public String getData(int tripID) {
		
		String result = "";
		
		//Save location and credentials of database
		String url = "jdbc:mysql://localhost:3306/";
		String dbname = "School_Trip_Manager";
		String username = "root";
		String password = "admin";
		
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		// Establish a connection to db, query db and save results in result set
		try {
			con = DriverManager.getConnection(url+dbname, username, password);
			
			String query = "SELECT * FROM attendees WHERE Trip_ID = ?";
			pstmt = con.prepareStatement(query);
			pstmt.setInt(1, tripID);
			rs = pstmt.executeQuery();
			
			//Store result set in variable to be returned by method
			while (rs.next()) {
				result += "TRIP ID: " + rs.getString("Trip_ID") + "\n" + "STUDENT ID: " + rs.getString("Student_ID") + "\n"
						+ "TRIP NAME: " + rs.getString("Trip_Name") + "\n" + "STUDENT NAME: " + rs.getString("Student_Name") + "\n"
						+ "OUTSTANDING FEES: " + rs.getString("Outstanding_fees") + "\n" + "*****************************" + "\n";
			}
			
		}
		catch (SQLException e) {e.printStackTrace();}
		catch (Exception e) {e.printStackTrace();}
		
		finally {
			if (rs != null) {
				try {rs.close();} catch (Exception e) {e.printStackTrace();}
			}
			if (pstmt != null) {
				try {pstmt.close();} catch (Exception e) {e.printStackTrace();}
			}
			if (con != null) {
				try {con.close();} catch (Exception e) {e.printStackTrace();}
			}
		}
		
		return result;
		
		


}
}
